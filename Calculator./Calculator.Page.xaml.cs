﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Calculator
{
    public partial class Calculator_Page : ContentPage
    {
        int stateOn = 1;
        string operation;
        double Num1, Num2;

        public Calculator_Page()
        {
            InitializeComponent();
            OnClickClear(this, null);
        }
        void OnClickNum(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string pressed = button.Text;

            if (this.ans.Text == "0" || stateOn < 0)
            {
                this.ans.Text = "";
                if (stateOn < 0)
                    stateOn *= -1;
            }

            this.ans.Text += pressed;

            double number;
            if (double.TryParse(this.ans.Text, out number))
            {
                this.ans.Text = number.ToString("N0");
                if (stateOn == 1)
                {
                    Num1 = number;
                }
                else
                {
                    Num2 = number;
                }
            }
        }
        void OnClickOperator(object sender, EventArgs e)
        {
            stateOn = -2;
            Button button = (Button)sender;
            string pressed = button.Text;
            operation = pressed;
        }

        void OnClickClear(object sender, EventArgs e)
        {
            Num1 = 0;
            Num2 = 0;
            stateOn = 1;
            this.ans.Text = "0";
        }

        void OnCalculate(object sender, EventArgs e)
        {
            if (stateOn == 2)
            {
                var result = Calculate(Num1, Num2, operation);
                this.ans.Text = result.ToString();
                Num1 = result;
                stateOn = -1;
            }
        }

        public static double Calculate(double Val1, double Val2, string operation)
        {
            double result = 0;

            if (operation == "+")
                result = Val1 + Val2;

            if (operation == "-")
                result = Val1 - Val2;

            if (operation == "X")
                result = Val1 * Val2;

            if (operation == "/")
                result = Val1 / Val2;

            return result;
        }

    }
}
